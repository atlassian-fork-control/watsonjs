export default class LoadAverages {
  public oneMinute!: number;
  public fiveMinutes!: number;
  public fifteenMinutes!: number;
}
