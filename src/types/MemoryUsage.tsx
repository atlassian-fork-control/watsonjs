export default class MemoryUsage {
  public memoryTotal!: number;
  public memoryUsed!: number;
  public memoryFree!: number;
  public swapTotal!: number;
  public swapUsed!: number;
  public swapFree!: number;
}
